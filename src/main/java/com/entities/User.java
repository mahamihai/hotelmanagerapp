package com.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

@Table (name="user")
public class User {

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAcces() {
		return acces;
	}

	public void setAcces(int acces) {
		this.acces = acces;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="userId")
	private int userId;


	@Column(name="name")
	private String name;



	@Column(name="password")
	private String password;
	
	@Column(name="acces")
	private int acces;
	


	
	public String toString() {
		String v="Name " + this.name + ", "
                + "Password  " + this.password+" , ID:"+this.userId;
		return v;
    }
}
