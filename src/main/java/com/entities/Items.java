package com.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="produse")
public class Items {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="produs_id")
	private Integer produs_id;
	
	@Column(name="produs_name")
	private String produs_name;
	
	@Column(name="produs_categorie")
	private String produs_categorie;
		
	@Column(name="pret")
	private Integer pret;

	public Integer getProdus_id() {
		return produs_id;
	}

	public void setProdus_id(Integer produs_id) {
		this.produs_id = produs_id;
	}

	public String getProdus_name() {
		return produs_name;
	}

	public void setProdus_name(String produs_name) {
		this.produs_name = produs_name;
	}

	public String getProdus_categorie() {
		return produs_categorie;
	}

	public void setProdus_categorie(String produs_categorie) {
		this.produs_categorie = produs_categorie;
	}

	public Integer getPret() {
		return pret;
	}

	public void setPret(Integer pret) {
		this.pret = pret;
	}
	
	

}
