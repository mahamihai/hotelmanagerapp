package com.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import com.entities.User;
import com.services.UserServices;

@Controller

public class LoginController {

	@Autowired
    UserServices userServices;
	
	@RequestMapping(path = "/login")
    public String showLoginForm()

	{
		return "login";
    }
	
	
	@RequestMapping(path = "/login", method = RequestMethod.POST)
    public String verifyLogin(@RequestParam String name,@RequestParam String password, 
    		HttpSession session,Model model) {
	
		User user=userServices.loginUser(name, password);
		if (user==null) {
			System.out.println("login control user null");
			model.addAttribute("loginError","Error logging in. Please try again");
			return "login";
		}
		session.setAttribute("loggedInUser", user);
		session.setAttribute("loggedInUserA", user.getAcces());
		return "admin";
	}
}
