package com.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.entities.Items;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.entities.Room;
import com.services.RoomServices;


@Controller
@RequestMapping(value="room")
public class RoomController {
	
	@Autowired
    RoomServices roomServices;
	
 
    @RequestMapping(path = "/page", method = RequestMethod.GET)
    public ModelAndView getPage() {
        ModelAndView view = new ModelAndView("roomsPage");
        return view;
    }
 
    @RequestMapping(path = "/saveOrUpdate", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> getSaved(@RequestBody Room room) {
        Map<String, Object> map = new HashMap<String, Object>();
 
        if (roomServices.saveOrUpdate(room)) {
            map.put("status", "200");
            map.put("message", "Your record have been saved successfully");
        }
 
        return map;
    }
    @RequestMapping(path = "/delete", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> delete(@RequestBody Room room) {
        Map<String, Object> map = new HashMap<String, Object>();

        if (roomServices.delete(room)) {
            map.put("status", "200");
            map.put("message", "Your room have been deleted successfully");
        }

        return map;
    }
    @RequestMapping(path = "/list", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> getAll(Room room) {
        Map<String, Object> map = new HashMap<String, Object>();
 
        List<Room> list = roomServices.list();
 
        if (list != null) {
            map.put("status", "200");
            map.put("message", "Data found");
            map.put("data", list);
        } else {
            map.put("status", "404");
            map.put("message", "Data not found");
 
        }
 
        return map;
    }

}
