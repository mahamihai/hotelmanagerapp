package com.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.User;
import com.services.UserServices;



@Controller
@RequestMapping(value="user")
public class UserController {
	
	@Autowired
    UserServices userServices;
	

    @RequestMapping(path = "saveOrUpdate", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> getSaved(@RequestBody User user) {
        Map<String, Object> map = new HashMap<String, Object>();
 
        if (userServices.saveOrUpdate(user)) {
            map.put("status", "200");
            map.put("message", "Your record have been saved successfully");
        }
 
        return map;
    }
 
    @RequestMapping(path = "/list", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> getAll(User user) {
        Map<String, Object> map = new HashMap<String, Object>();
 
        List<User> list = userServices.list();
 
        if (list != null) {
            map.put("status", "200");
            map.put("message", "Data found");
            map.put("data", list);
        } else {
            map.put("status", "404");
            map.put("message", "Data not found");
 
        }
 
        return map;
    }
 
    @RequestMapping(path = "/delete", method = RequestMethod.DELETE)
    public @ResponseBody Map<String, Object> delete(@RequestBody User user) {
        Map<String, Object> map = new HashMap<String, Object>();
 
        if (userServices.delete(user)) {
        	System.out.println("UserController "+user.getUserId());
        	map.put("status", "200");
            map.put("message", "Your record have been deleted successfully");
        }
 
        return map;
    }
    
    @RequestMapping(path = "/page1", method = RequestMethod.GET)
    public ModelAndView getPage1() {
        ModelAndView view = new ModelAndView("user1");
        return view;
    }

}
