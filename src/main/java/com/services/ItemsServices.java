package com.services;

import java.util.List;

import com.entities.Items;

public interface ItemsServices {
	
	 List<Items> list();
	 boolean delete(Items item);
	 boolean saveOrUpdate(Items item);
}
