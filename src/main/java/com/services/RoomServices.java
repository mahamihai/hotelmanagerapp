package com.services;

import java.util.List;

import com.entities.Items;
import com.entities.Room;

public interface RoomServices {
	
	public List<Room> list();
	public boolean saveOrUpdate(Room room);
	boolean delete(Room item);

}
