package com.services;

import com.entities.User;

import java.util.List;



public interface UserServices {
	public List<User> list();
	public boolean delete(User user);
	public boolean saveOrUpdate(User user);
	public User loginUser(String name,String password);
}
