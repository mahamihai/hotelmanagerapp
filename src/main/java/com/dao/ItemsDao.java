package com.dao;

import java.util.List;

import com.entities.Items;
import com.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemsDao extends JpaRepository<Items, Long> {


}
