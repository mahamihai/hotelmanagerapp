package com.dao;

import java.util.List;

import com.entities.Room;
import com.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomDao  extends JpaRepository<Room, Long> {

}
