package com.servicesImpl;

import java.util.List;

import com.entities.Items;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.RoomDao;
import com.entities.Room;
import com.services.RoomServices;

@Service
public class RoomServicesImpl implements RoomServices{

	@Autowired
	RoomDao roomDao;
	
	@Override
	public List<Room> list() {
		// TODO Auto-generated method stub
		return roomDao.findAll();
	}

	@Override
	public boolean saveOrUpdate(Room room) {
		// TODO Auto-generated method stub
		 roomDao.save(room);
		return true;
	}
	@Override
	public boolean delete(Room item) {
		// TODO Auto-generated method stub
		roomDao.delete(item);
		return true;
	}

}
