package com.servicesImpl;

//import java.util.ArrayList;
import java.util.List;
//import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entities.Items;
import com.services.ItemsServices;

import com.dao.ItemsDao;

@Service
public class ItemsServicesImpl implements ItemsServices {

	@Autowired
	ItemsDao itemsDao;
	
	//List<Items> ls = new ArrayList<Items>();
	//long expira =0;
	@Override
	public List<Items> list() {
		// TODO Auto-generated method stub
		return itemsDao.findAll();
		}
	/*public List<Items> list() {
		// TODO Auto-generated method stub
		
		long timeMilis = System.currentTimeMillis();
		long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(timeMilis);
		long diff;
		
		if (ls.size()>0 && timeSeconds< expira ) {
			System.out.println("date luate din cache");
		} else if(ls.size()>0 && timeSeconds>= expira) {
			ls.clear();
			System.out.println("date luate din BD");
			ls=itemsDao.list();
			expira=timeSeconds+30;
		}else if(ls.size()==0) {
			System.out.println("date luate din BD");
			ls=itemsDao.list();
			expira=timeSeconds+30;
		}
		return ls;
	}
*/
	@Override
	public boolean delete(Items item) {
		// TODO Auto-generated method stub
		 itemsDao.delete(item);
		return true;
	}

	@Override
	public boolean saveOrUpdate(Items item) {
		// TODO Auto-generated method stub
		 itemsDao.save(item);
		return true;
	}

}
