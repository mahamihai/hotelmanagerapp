package com.servicesImpl;

import com.dao.UserDao;
import com.entities.User;
import com.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServicesImpl implements UserServices {
	
	@Autowired
	UserDao userDao;
	@Override
	public List<User> list() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}

	@Override
	public boolean delete(User user) {
		// TODO Auto-generated method stub
		System.out.println("UserServices "+user.getUserId()+"  "+user.getName() +"  "+user.getPassword() + "  "+user.getAcces());
		 userDao.delete(user);
		 return true;
	}

	@Override
	public boolean saveOrUpdate(User user) {
		// TODO Auto-generated method stub
		System.out.println("UserServices "+user.getUserId()+"  "+user.getName() +"  "+user.getPassword() + "  "+user.getAcces());
		this.userDao.save(user);
		return true;
	}
	
	public User loginUser(String name, String password) {
		List<User> users=userDao.findAll();
		User found=null;
		try {
			found = users.stream().filter(x -> x.getPassword().equals(password) && x.getName().equals(name)).findFirst().get();
		}
		catch(Exception e){
			System.out.println(e.toString());
			return null;
		}
		if (found != null) {
			System.out.println("User Login"+found.getUserId()+"  "+found.getName());
			return found;
		}
		return null;
	}

}
