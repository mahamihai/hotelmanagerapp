<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Rooms</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>
<body onload="load();">
 
        <input type="hidden" id="room_id">
        Name:   <input type="text" id="name" required="required" name="room_name"><br>
        Status: <input type="text" id="status" required="required" name="room_status"><br>
        Date:   <input type="date" id="date" required="required" name="date"><br>
        Pret:   <input type="text" id="pret" required="required" name="pret"><br>
        <button onclick="submit();">Submit</button>
     
     
 
        <table id="table" border=1>
            <tr> <th> Name </th> <th> Status </th> <th> Date </th> <th> Pret </th> <th> Edit </th><th>Delete</th> </tr>
         
        </table>
             
     
    <script type="text/javascript">
    data = "";
    submit = function(){
        var data={};
        data["room_id"]=document.getElementById("room_id").value;
        data["room_name"]=document.getElementById("name").value;

        data["room_status"]=document.getElementById("status").value;
        data["date"]=document.getElementById("date").value;
        data["pret"]=document.getElementById("pret").value;


        $.ajax({
            contentType: "application/json",

            url:'http://localhost:8080/room/saveOrUpdate',
                type:'POST',
                data:JSON.stringify(data),
                success: function(response){
                        alert(response.message);
                        load();    
                }              
            });        
    }

    delete_ = function(id){
        var data={};
        data["room_id"]=id;
        console.log('sterg camera', data);

        $.ajax({
            url:'http://localhost:8080/room/delete',
            type:'POST',
            contentType: "application/json",

            data:JSON.stringify(data),
            success: function(response){
                alert(response.message);
                load();
            }
        });
    }



    edit = function (index){
        $("#room_id").val(data[index].room_id);
        $("#name").val(data[index].room_name);
        $("#status").val(data[index].room_status);
        $("#date").val(data[index].date);
        $("#pret").val(data[index].pret);
         
    }
     
     
    load = function(){ 
        $.ajax({
            url:'http://localhost:8080/room/list',
            type:'POST',
            success: function(response){
                    data = response.data;
                    $('.tr').remove();
                    for(i=0; i<response.data.length; i++){                  
                        $("#table").append("<tr class='tr'> <td> "+response.data[i].room_name+" </td> <td> "+response.data[i].room_status+" </td> <td> "+response.data[i].date+" </td>  <td> "+response.data[i].pret+" </td>  <td> <a href='#' onclick= edit("+i+");> Edit </a>  </td> </td><td> <a href='#' onclick='delete_("+response.data[i].room_id+");'> Delete </a>  </td> </tr>");
                    }          
            }              
        });
         
    }
         
    </script>
     
     
      <h2>
          <a href="/">Back!!!</a>
    </h2>
</body>
</html>